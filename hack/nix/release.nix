# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

{ lib, pythonPackages, ... }:

let
  buildPythonPackage = pythonPackages.buildPythonPackage;
in

with pythonPackages;

buildPythonPackage rec {
  pname = "gitlabci";
  version = "0.0.1";

  src = ./.;

  checkInputs = [ pytest ];
  propagatedBuildInputs = [
    click
    sh
    boto3
   # coloredlogs
    kubernetes
    paramiko
    pyyaml
  ];

  meta = with lib; {
    homepage = "https://gitlab.com/cip-playground/gitlab-cloud-ci";
    description = "Setup for ISAR CI";
    license = "Proprietary";
    maintainers = "Michael Adler <michael.adler@siemens.com>";
  };
}
