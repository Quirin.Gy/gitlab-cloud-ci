# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

let

  nixpkgs = builtins.fromJSON (builtins.readFile ./nixpkgs.json);
  bootstrap = import <nixpkgs> { };

  src = bootstrap.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs";
    inherit (nixpkgs) rev sha256;
  };

  config = {
    packageOverrides = pkgs: rec {
      python37Packages = pkgs.python37Packages.override {
        overrides = self: super: rec {
          # obsolete once https://github.com/amoffat/sh/pull/468/ is merged
          sh = super.sh.overridePythonAttrs(old: { doCheck = false; } );
        };
      };
    };
  };

  pkgs = import src { inherit config; };

in

{

  gitlabcloudci = pkgs.callPackage ./release.nix {
    pythonPackages = pkgs.python37Packages;
  };

}
