This fork of gitlab-runner contains a workaround for [gitlab-runner/issues/4125](https://gitlab.com/gitlab-org/gitlab-runner/issues/4125).

It is (mostly) backwards compatible with the upstream gitlab-runner.
However, it tries to respect the container's entrypoint in a best-effort manner:

1. If `/entrypoint` exists and is executable, execute it.
2. Otherwise, if `/kas/docker-entrypoint` exists and is executable, execute it.

Currently, we expect the entrypoint script to be idempotent.
