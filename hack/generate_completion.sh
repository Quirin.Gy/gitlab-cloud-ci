#!/usr/bin/env sh
# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

case $1 in
    bash)
        _GITLABCI_COMPLETE=source ./gitlabci
        ;;
    zsh)
        _GITLABCI_COMPLETE=source_zsh ./gitlabci
        ;;
esac
