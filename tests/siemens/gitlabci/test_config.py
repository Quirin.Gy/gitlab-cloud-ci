# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

from siemens.gitlabci.config import Config


def test_load_config():
    cfg = Config("./config.json.sample")
    aws_keys = ["cluster_name", "ssh_key", "asg_policy_name", "region"]
    for key in aws_keys:
        assert key in cfg.aws
    assert len(cfg.aws) == len(aws_keys)
