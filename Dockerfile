# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

FROM python:3.7-alpine

COPY requirements.txt /tmp/requirements.txt

ENV K8S_VERSION 1.14.9
ENV KOPS_VERSION 1.14.1

RUN apk --update add --no-cache bash bash-completion openssl libffi ca-certificates make \
  && apk --update add --virtual build-dependencies gcc musl-dev openssl-dev libffi-dev curl \
  && pip3 install --no-cache-dir -r /tmp/requirements.txt \
  && rm -f /tmp/requirements.txt \
  && curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash \
  && curl -sL https://dl.k8s.io/v${K8S_VERSION}/kubernetes-client-linux-amd64.tar.gz | tar x -z -C /tmp && mv /tmp/kubernetes/client/bin/kubectl /usr/local/bin/kubectl \
  && curl -o /usr/local/bin/kops -sL https://github.com/kubernetes/kops/releases/download/${KOPS_VERSION}/kops-linux-amd64 \
  && chmod 755 /usr/local/bin/kops \
  && mkdir -p /etc/bash_completion.d \
  && kubectl completion bash > /etc/bash_completion.d/kubectl \
  && helm completion bash > /etc/bash_completion.d/helm \
  && kops completion bash > /etc/bash_completion.d/kops \
  && apk del build-dependencies

EXPOSE 8001/tcp

ENTRYPOINT ["/bin/bash", "-c"]
