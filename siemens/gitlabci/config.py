# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import json


class Config:
    def __init__(self, fpath):
        with open(fpath) as f:
            cfg = json.load(f)
        self.aws = {
            "cluster_name": cfg["aws"]["cluster_name"],
            "ssh_key": cfg["aws"]["ssh_key"],
            "asg_policy_name": cfg["aws"]["asg_policy_name"],
            "region": cfg["aws"]["region"],
        }

    def __str__(self):
        return "Config: { aws: %s }" % self.aws
