# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os
import logging
import click

from siemens.gitlabci.cli import destroy

if not os.getenv("COMP_WORDS", None):
    from sh import kubectl
    from siemens.gitlabci.lib.ssh import SSHSession

log = logging.getLogger(__name__)


@destroy.command()
@click.pass_context
@click.option(
    "--config-file",
    default="~/.ssh/config",
    help="location of the ssh configuration file",
    show_default=True,
)
@click.option(
    "--sudo",
    is_flag=True,
    help="use sudo to create the Kubernetes cluster",
    show_default=False,
)
@click.option(
    "--delete-iptables",
    is_flag=True,
    help="delete iptables settings",
    show_default=False,
)
@click.option(
    "--do-not-drain-pods",
    is_flag=True,
    help="delete iptables settings",
    show_default=False,
)
@click.option(
    "--ask-password", is_flag=True, help="ask for ssh passphrase", show_default=False
)
@click.argument("destination")
def ssh(
    ctx,
    destination,
    config_file,
    sudo,
    delete_iptables,
    do_not_drain_pods,
    ask_password,
):
    """creates a ISAR CI setup running on the given host"""
    if not do_not_drain_pods:
        kubectl(
            "drain",
            "k8s-master",
            "--delete-local-data",
            "--force",
            "--ignore-daemonsets",
        )
        kubectl("delete", "pods", "--all")
    ssh_password = None
    if ask_password:
        ssh_password = click.prompt(
            "enter ssh passphrase:", hide_input=True, confirmation_prompt=True
        )
    ssh_session = SSHSession(destination, config_file, ssh_password)
    try:
        sudo_cmd = ""
        if sudo:
            sudo_cmd = "sudo "
        destroy_cmd = "{sudo} kubeadm reset".format(sudo=sudo_cmd)
        # direct access for input
        ssh_session.execute_command(destroy_cmd, "y\n")
        if delete_iptables:
            delete_iptables_cmd = "{sudo} sh -c 'iptables -F && \
           iptables -t nat -F && iptables -t mangle -F && iptables -X'".format(
                sudo=sudo_cmd
            )

            ssh_session.execute_command(delete_iptables_cmd)
    finally:
        ssh_session.close()
