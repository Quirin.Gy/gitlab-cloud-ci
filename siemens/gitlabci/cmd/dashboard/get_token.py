# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

from siemens.gitlabci.cli import dashboard as cli

if not os.getenv("COMP_WORDS", None):
    import base64
    import kubernetes

    from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client

log = logging.getLogger(__name__)


@cli.command()
def get_token():
    """Get token to login to K8s dashboard"""

    log.setLevel(logging.ERROR)

    configure_kubernetes_client()
    k8s_client_core = kubernetes.client.CoreV1Api()
    for item in k8s_client_core.list_namespaced_secret("kube-system").items:
        metadata = item.metadata
        annotations = metadata.annotations
        if (
            annotations
            and annotations["kubernetes.io/service-account.name"]
            == "kubernetes-dashboard"
        ):
            print("=" * 79)
            print(base64.b64decode(item.data["token"]).decode())
            print("=" * 79)
