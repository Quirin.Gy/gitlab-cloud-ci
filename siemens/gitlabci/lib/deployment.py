# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

from sh import helm


def bootstrap_helm():
    helm("repo", "add", "stable", "https://kubernetes-charts.storage.googleapis.com/")
    helm("repo", "update")
