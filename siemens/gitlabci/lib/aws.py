# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging

import boto3
import botocore.exceptions

log = logging.getLogger(__name__)


def check_aws_response(response):
    status_code = response["ResponseMetadata"]["HTTPStatusCode"]
    if not 200 <= status_code < 300:
        raise RuntimeError("Error from AWS: %s", response)


def s3_exists_bucket(s3, bucket_name):
    try:
        s3.meta.client.head_bucket(Bucket=bucket_name)
        return True
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the bucket does not exist.
        if e.response["Error"]["Code"] == "404":
            return False
        else:
            raise e


def aws_iam_lookup_policy_arn(client, policy_name):
    response = client.list_policies(Scope="Local")
    check_aws_response(response)
    for policy in response["Policies"]:
        if policy["PolicyName"] == policy_name:
            return policy["Arn"]
    return None


def delete_autoscaler_iam(cluster_name, region, asg_policy_name):
    full_cluster_name = "{}.k8s.local".format(cluster_name)
    iam_role = "masters.{}".format(full_cluster_name)
    iam = boto3.client("iam", region_name=region)
    policy_arn = aws_iam_lookup_policy_arn(iam, asg_policy_name)
    if policy_arn:
        log.info("Detaching policy %s from role %s", policy_arn, iam_role)
        try:
            check_aws_response(
                iam.detach_role_policy(RoleName=iam_role, PolicyArn=policy_arn)
            )
        except Exception as e:
            log.warning(e)

        log.info("Deleting policy %s", policy_arn)
        check_aws_response(iam.delete_policy(PolicyArn=policy_arn))
