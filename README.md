# Gitlab Custom Cloud CI

This project creates a container-based CI-infrastructure for Gitlab from scratch.

Objectives:

* Scale horizontally: run an arbitrary number of CI jobs in parallel
* Scale dynamically: scale up and down based on the current workload situation
* Support running **privileged containers**  for CI pipelines (unlike the default Gitlab runners). This is the main reason this project exists.

The objectives are met by using

* AWS as infrastructure provider
* Kubernetes to orchestrate CI runners and fulfill the scaling demands

Thanks to Kubernetes auto-scaling, this setup can easily perform **thousands** of concurrent CI builds.
The only limiting factor is your budget to pay for AWS infrastructure.

## Preparation

* An `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are needed to create the necessary AWS resources.
The following permissions must be associated with the API keys:
    * `AmazonEC2FullAccess`
    * `AmazonRoute53FullAccess`
    * `AmazonS3FullAccess`
    * `IAMFullAccess`
    * `AmazonVPCFullAccess`

See [github.com/kubernetes/kops/blob/master/docs/aws.md](https://github.com/kubernetes/kops/blob/master/docs/aws.md)  for detailed instructions.
* A valid ssh public key in `config.json`

## Running gitlabci

It is recommended to execute `gitlabci` within the provided Docker container:

* Build the Docker base image: `make docker`
* Run `make shell` to create a container and enter an interactive shell
* From that point forward, `gitlabci --help` is your friend - or use the provided bash completion.

## Create Cluster

Clusters can run on either AWS or on a Linux host (ssh root access required).
Copy `config.json.sample` to `config.json` and adjust the configuration accordingly.

Then run:

```bash
# create cluster on AWS
$ gitlabci create aws --node-size m5d.xlarge

# optionally, deploy auto-scaler addon
# by default, unneeded nodes are removed after being idle for *1* minute.
$ gitlabci autoscaler deploy --min-size 0 --max-size 3

# or, alternatively on-premise:
$ gitlabci create ssh
```

See `gitlabci create --help` for all available options.

To tear down the cluster and all resources associated with it:

```bash
$ gitlabci destroy aws
# or, alternatively on-premise:
$ gitlabci destroy ssh
```

### SSH

Prerequisites:

- The SSH keys used by `gitlabci` need to be in PEM format.
  You can convert keys with: `ssh-keygen -p -m PEM -f <private_key>` or generate a new one with `ssh-keygen -t rsa -m PEM`.
  The file `~/.ssh/config` is partially supported to determine SSH connection settings.
- Ensure iptables tooling does not use the nftables backend: See [legacy iptables](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#ensure-iptables-tooling-does-not-use-the-nftables-backend).
- [docker-ce](https://kubernetes.io/docs/setup/production-environment/container-runtimes/#docker)
- [kubeadm, kubectl, kubelet](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-runtime)
- Swap must be deactivated (`swapoff -a`)

## Deploy Gitlab Runner

You can deploy as many Gitlab runners as you like.

Go to your projects CI setup page (Settings -> CI/CD -> Runners) and grab the token for Runner registration.
Then, for each runner which you want to deploy, create a local copy of ``share/k8s/gitlab-runner/values.yaml`, adjust the configuration and deploy the runner, e.g.

```bash
$ gitlabci runner deploy --token=$GITLAB_CI_TOKEN --config=share/k8s/gitlab-runner/cip/cip-project-small.yaml cip-project-small
```

To purge a Gitlab runner from your cluster, run

```bash
$ gitlabci runner purge gitlab-cloud-ci
```

Hint: Use `gitlabci runner list` to get an overview of all deployed runners.

## Deploy Kubernetes Dashboard

A Kubernetes dashboard can be deployed as well and accessed as follows:

```bash
$ gitlabci dashboard deploy
$ kubectl proxy --address=0.0.0.0 &
$ gitlabci dashboard get-token
```

Now open
http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:https/proxy/
in your browser and use the above token to login.

## Custom Docker Registries

In order to use a Docker registry requiring authentication, create the file `./secrets/docker.json` with the following contents:

```json
{
  "auths": {
    "registry.gitlab.com": {
      "auth": "<base64 encoded value of user:token>"
    }
  }
}
```

Then run

```bash
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=./secrets/docker.json \
    --type=kubernetes.io/dockerconfigjson \
    --namespace gitlab
```

See https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/ for further details.

## Cluster Management & Fine-Tuning

Typically you will want to fine-tune certain cluster settings (e.g. node instance type) because the current settings do not properly fit your workload.

`gitlabci` only exposes a (currently small) subset of the available cluster management commands.

However, `make shell` will start a Docker container in which many tools such as `helm` are available (and functional) without any extra configuration,
e.g. commands like `helm list` just work out of the box.

### kubectl

**AWS**: In case you have lost your local kube config file, you can simply re-create it using  `kops export kubecfg "${CLUSTER_NAME}.k8s.local"`.
(This file is needed to invoke `kubectl`.)

### Modify Gitlab Runners

For instance, to upgrade a Gitlab Runner to the latest version or modify some configuration settings:

```bash
$ helm repo update
# remove '--dry-run' if output looks sane
$ helm upgrade isar gitlab/gitlab-runner --dry-run --reuse-values -f ./share/k8s/gitlab-runner/values.yaml
```

### Modify Auto-Scaling Group Configuration

You can adjust the configuration of the auto-scaling group either in the AWS UI or using your favorite command-line tool, e.g.

```bash
$ awless list scalinggroups --format json
$ awless update scalinggroup name=... max-size=10
```
### Modify EC2 instance type for nodes

```bash
kops edit ig --name ${CLUSTER_NAME}.k8s.local nodes
kops update cluster ${CLUSTER_NAME}.k8s.local --yes
```

**Notice:** You have to re-deploy the `cluster-autoscaler` plugin afterwards because kops overrides certain tags in the ASG configuration.

## Testing

To test the on-premise setup you can use the provided Vagrantfile in `./test/siemens/onpremise/`.
See [README.vagrant.md](./tests/siemens/onpremise-vm/README.vagrant.md) for further instructions.

## References

* https://docs.gitlab.com/runner/executors/kubernetes.html
* https://docs.gitlab.com/runner/install/kubernetes.html
* https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
* https://github.com/kubernetes/autoscaler
